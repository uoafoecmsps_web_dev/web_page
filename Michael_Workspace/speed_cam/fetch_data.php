<!DOCTYPE html> 
<html>
  <head>
    <title>Fetch</title>
  </head> 
  <body>
    <?php

      // cron job on go daddy: wget http://www.aecmspss.org/speed_cam/test2.php > /dev/null 2>&1

      date_default_timezone_set("Australia/Adelaide");

      $today = date('N', strtotime(date("l")));

      include_once './simplehtmldom_1_5/simple_html_dom.php';

      $html = file_get_html('https://www.police.sa.gov.au/your-safety/road-safety/traffic-camera-locations');
      $locations = $html->find('div[class=accordion]');
      $today_loc = $locations[$today-1]->next_sibling();
      
      // p is an array
      $loc = $today_loc->find('p');
      $res_loc = "var locations = [";
      foreach ($loc as $res) {
          $res_loc = $res_loc . '"' .  strip_tags($res) . '", '; 
      }
      $res_loc = $res_loc . "];";

      echo $res_loc . "<br><br>";

      $fixed_camera = $html->find('h2[id=fixedcamera]');
      $fixed_camera = $fixed_camera[0]->parent()->next_sibling()->next_sibling();
      $camera_table = $fixed_camera->find('tr');

      $res_fix = "var fixed = [";
      foreach ($camera_table as $res) {
      	  $td = $res->find('td');
      	  $test = strtolower(str_replace(' ', '', strip_tags($td[2])));
      	  if(strcmp($test, "i/section") == 0)
      	  {
            $temp = trim(strip_tags($td[1]));
            $temp = str_replace('/', ' & ', $temp);
            $temp = str_replace("'", "''", $temp);
          	$res_fix = $res_fix . '"' .  $temp . '", '; 
          }
      }
      $res_fix = $res_fix . "];";

      echo $res_fix . "<br><br>";

      $conn = new mysqli('localhost', 'MichaelZhu', 'Zcc88929@', 'speed_camera_location_history');
      if ($conn->connect_error)
      {
        die("Connection failed: " . $conn->connect_error);
      }

      $now = date("Y-m-d H:i:s");
      $sql = "INSERT INTO location_2016 (date, location, fixed) VALUES ('$now', '$res_loc', '$res_fix')";

      if ($conn->query($sql) === TRUE) {
          echo "New record created successfully<br><br>";
      } else {
          echo "Error: " . $sql . "<br>" . $conn->error;
      }

      $result = $conn->query("SELECT * FROM location_2016 ORDER BY id DESC LIMIT 1");

      if ($result->num_rows > 0) 
      {
        while($row = $result->fetch_assoc())
        {
          echo "<br/>ID: " . $row["id"] . "<br/><br/> Date: " . $row["date"] . "<br/><br/> mobile: " . $row["location"] . "<br/><br/> fixed: " . $row["fixed"] . "<br/>";
        }
      }
      else 
      {
        echo "0 results";
      }

      $conn->close();

    ?>
  </body>
</html>

