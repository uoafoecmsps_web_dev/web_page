<!DOCTYPE html> 
<!--[if IE 8]>			<html class="ie ie8"> <![endif]-->
<!--[if IE 9]>			<html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->	<html> <!--<![endif]-->
	<head>
		<meta charset="utf-8" />
		<title>I-Life: Adelaide Mobile Speeding Cameras Locator</title>
		<meta name="keywords" content="HTML5,CSS3,Template" />
		<meta name="description" content="" />
		<meta name="Author" content="Dorin Grigoras [www.stepofweb.com]" />

		<!-- mobile settings -->
		<meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0" />

		<!-- WEB FONTS -->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700,800" rel="stylesheet" type="text/css" />

		<!-- CORE CSS -->
		<link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
		<link href="assets/css/font-awesome.css" rel="stylesheet" type="text/css" />
		<link href="assets/plugins/owl-carousel/owl.carousel.css" rel="stylesheet" type="text/css" />
		<link href="assets/plugins/owl-carousel/owl.theme.css" rel="stylesheet" type="text/css" />
		<link href="assets/plugins/owl-carousel/owl.transitions.css" rel="stylesheet" type="text/css" />
		<link href="assets/plugins/magnific-popup/magnific-popup.css" rel="stylesheet" type="text/css" />
		<link href="assets/css/animate.css" rel="stylesheet" type="text/css" />
		<link href="assets/css/superslides.css" rel="stylesheet" type="text/css" />

		<!-- REVOLUTION SLIDER -->
		<link href="assets/plugins/revolution-slider/css/settings.css" rel="stylesheet" type="text/css" />

		<!-- THEME CSS -->
		<link href="assets/css/essentials.css" rel="stylesheet" type="text/css" />
		<link href="assets/css/layout.css" rel="stylesheet" type="text/css" />
		<link href="assets/css/layout-responsive.css" rel="stylesheet" type="text/css" />
		<link href="assets/css/color_scheme/orange.css" rel="stylesheet" type="text/css" /><!-- orange: default style -->
		<!--<link id="css_dark_skin" href="assets/css/layout-dark.css" rel="stylesheet" type="text/css" />--><!-- DARK SKIN -->

		<!-- styleswitcher - demo only -->
		<link href="assets/css/color_scheme/orange.css" rel="alternate stylesheet" type="text/css" title="orange" />
		<link href="assets/css/color_scheme/red.css" rel="alternate stylesheet" type="text/css" title="red" />
		<link href="assets/css/color_scheme/pink.css" rel="alternate stylesheet" type="text/css" title="pink" />
		<link href="assets/css/color_scheme/yellow.css" rel="alternate stylesheet" type="text/css" title="yellow" />
		<link href="assets/css/color_scheme/darkgreen.css" rel="alternate stylesheet" type="text/css" title="darkgreen" />
		<link href="assets/css/color_scheme/green.css" rel="alternate stylesheet" type="text/css" title="green" />
		<link href="assets/css/color_scheme/darkblue.css" rel="alternate stylesheet" type="text/css" title="darkblue" />
		<link href="assets/css/color_scheme/blue.css" rel="alternate stylesheet" type="text/css" title="blue" />
		<link href="assets/css/color_scheme/brown.css" rel="alternate stylesheet" type="text/css" title="brown" />
		<link href="assets/css/color_scheme/lightgrey.css" rel="alternate stylesheet" type="text/css" title="lightgrey" />
		<!-- /styleswitcher - demo only -->
		
		

		<!-- STYLESWITCHER - REMOVE ON PRODUCTION/DEVELOPMENT -->
		<link href="assets/plugins/styleswitcher/styleswitcher.css" rel="stylesheet" type="text/css" />		

		<!-- Morenizr -->
		<script type="text/javascript" src="assets/plugins/modernizr.min.js"></script>
	</head>
	<body><!-- Available classes for body: boxed , pattern1...pattern10 . Background Image - example add: data-background="assets/images/boxed_background/1.jpg"  -->



		<!-- TOP NAV -->
		<header id="topNav">
			<div class="container">

				<!-- Mobile Menu Button -->
				<button class="btn btn-mobile" data-toggle="collapse" data-target=".nav-main-collapse">
					<i class="fa fa-bars"></i>
				</button>

				<!-- Logo text or image -->
				<a class="logo scrollTo" href="#wrapper">
					<strong>I-Life</strong>
				</a>



				<!-- Top Nav -->
				<div class="navbar-collapse nav-main-collapse collapse pull-right">
					<nav class="nav-main mega-menu">
						<ul class="nav nav-pills nav-main scroll-menu" id="topMain">
							<!--<li class="active"><a href="#wrapper">Home</a></li>-->
							<li class="active"><a href="#camera_map">Camera Map</a></li>
							<li><a href="#support">Support Us</a></li>
							<li><a href="#contact">Contact Us</a></li>
						</ul>
					</nav>
				</div>
				<!-- /Top Nav -->

			</div>
		</header>

		<span id="header_shadow"></span>
		<!-- /TOP NAV -->



		<!-- STYLESWITCHER - REMOVE ON PRODUCTION/DEVELOPMENT
		<div id="switcher">
			<div class="content-switcher" >        
				<h4>STYLE OPTIONS</h4>

				<p>10 Predefined Color Schemes</p>
				<ul>            
					<li><a href="#" onclick="setActiveStyleSheet('orange'); return false;" title="orange" class="color"><img src="assets/images/demo/color_schemes/1.png" alt="" width="30" height="30" /></a></li>
					<li><a href="#" onclick="setActiveStyleSheet('red'); return false;" title="red" class="color"><img src="assets/images/demo/color_schemes/2.png" alt="" width="30" height="30" /></a></li>
					<li><a href="#" onclick="setActiveStyleSheet('pink'); return false;" title="pink" class="color"><img src="assets/images/demo/color_schemes/3.png" alt="" width="30" height="30" /></a></li>
					<li><a href="#" onclick="setActiveStyleSheet('yellow'); return false;" title="yellow" class="color"><img src="assets/images/demo/color_schemes/4.png" alt="" width="30" height="30" /></a></li>
					<li><a href="#" onclick="setActiveStyleSheet('darkgreen'); return false;" title="darkgreen" class="color"><img src="assets/images/demo/color_schemes/5.png" alt="" width="30" height="30" /></a></li>
					<li><a href="#" onclick="setActiveStyleSheet('green'); return false;" title="green" class="color"><img src="assets/images/demo/color_schemes/6.png" alt="" width="30" height="30" /></a></li>
					<li><a href="#" onclick="setActiveStyleSheet('darkblue'); return false;" title="darkblue" class="color"><img src="assets/images/demo/color_schemes/7.png" alt="" width="30" height="30" /></a></li>
					<li><a href="#" onclick="setActiveStyleSheet('blue'); return false;" title="blue" class="color"><img src="assets/images/demo/color_schemes/8.png" alt="" width="30" height="30" /></a></li>
					<li><a href="#" onclick="setActiveStyleSheet('brown'); return false;" title="brown" class="color"><img src="assets/images/demo/color_schemes/9.png" alt="" width="30" height="30" /></a></li>
					<li><a href="#" onclick="setActiveStyleSheet('lightgrey'); return false;" title="lightgrey" class="color"><img src="assets/images/demo/color_schemes/10.png" alt="" width="30" height="30" /></a></li>
				</ul>        

				<p>CHOOSE YOUR COLOR SKIN</p>
				<label><input class="dark_switch" type="radio" name="color_skin" id="is_light" value="light" checked="checked" /> Light</label>
				<label><input class="dark_switch" type="radio" name="color_skin" id="is_dark" value="dark" /> Dark</label>

				<hr />

				<p>CHOOSE YOUR LAYOUT STYLE</p>
				<label><input class="boxed_switch" type="radio" name="layout_style" id="is_wide" value="wide" checked="checked" /> Wide</label>
				<label><input class="boxed_switch" type="radio" name="layout_style" id="is_boxed" value="boxed" /> Boxed</label>


				<p>Patterns for Boxed Version</p>
				<div>
					<button onclick="pattern_switch('none');" class="pointer switcher_thumb"><img src="assets/images/patterns/none.jpg" width="27" height="27" alt="" /></button>
					<button onclick="pattern_switch('pattern2');" class="pointer switcher_thumb"><img src="assets/images/patterns/pattern2.png" width="27" height="27" alt="" /></button>
					<button onclick="pattern_switch('pattern3');" class="pointer switcher_thumb"><img src="assets/images/patterns/pattern3.png" width="27" height="27" alt="" /></button>
					<button onclick="pattern_switch('pattern4');" class="pointer switcher_thumb"><img src="assets/images/patterns/pattern4.png" width="27" height="27" alt="" /></button>
					<button onclick="pattern_switch('pattern5');" class="pointer switcher_thumb"><img src="assets/images/patterns/pattern5.png" width="27" height="27" alt="" /></button>
				</div>

				<div>
					<button onclick="pattern_switch('pattern6');" class="pointer switcher_thumb"><img src="assets/images/patterns/pattern6.png" width="27" height="27" alt="" /></button>
					<button onclick="pattern_switch('pattern7');" class="pointer switcher_thumb"><img src="assets/images/patterns/pattern7.png" width="27" height="27" alt="" /></button>
					<button onclick="pattern_switch('pattern8');" class="pointer switcher_thumb"><img src="assets/images/patterns/pattern8.png" width="27" height="27" alt="" /></button>
					<button onclick="pattern_switch('pattern9');" class="pointer switcher_thumb"><img src="assets/images/patterns/pattern9.png" width="27" height="27" alt="" /></button>
					<button onclick="pattern_switch('pattern10');" class="pointer switcher_thumb"><img src="assets/images/patterns/pattern10.png" width="27" height="27" alt="" /></button>
				</div>

				<p>Images for Boxed Version</p>
				<button onclick="background_switch('none');" class="pointer switcher_thumb"><img src="assets/images/boxed_background/none.jpg" width="27" height="27" alt="" /></button>
				<button onclick="background_switch('assets/images/boxed_background/1.jpg');" class="pointer switcher_thumb"><img src="assets/images/boxed_background/1_thumb.jpg" width="27" height="27" alt="" /></button>
				<button onclick="background_switch('assets/images/boxed_background/2.jpg');" class="pointer switcher_thumb"><img src="assets/images/boxed_background/2_thumb.jpg" width="27" height="27" alt="" /></button>
				<button onclick="background_switch('assets/images/boxed_background/3.jpg');" class="pointer switcher_thumb"><img src="assets/images/boxed_background/3_thumb.jpg" width="27" height="27" alt="" /></button>
				<button onclick="background_switch('assets/images/boxed_background/4.jpg');" class="pointer switcher_thumb"><img src="assets/images/boxed_background/4_thumb.jpg" width="27" height="27" alt="" /></button>

				<hr />

				<div class="text-center">
					<button onclick="resetSwitcher();" class="btn btn-primary btn-xs">Reset Styles</button>
				</div>

				<div id="hideSwitcher">&times;</div>
			</div>
		</div>

		<div id="showSwitcher" class="styleSecondColor"><i class="fa fa-angle-double-right"></i></div>
		 /STYLESWITCHER -->



		<!-- WRAPPER -->
		<div id="wrapper">

			<!-- CAMERA_MAP -->
			<section id="camera_map" class="padding100">
				<div class="container">

					<h1 class="text-center">Mobile Speeding Cameras Locator</h1>
					<div class="divider"><!-- divider -->
						<i class="fa fa-star"></i>
					</div>

					<p class="lead">We pull data from SA Police's official data and display the estimated locations of today's mobile speeding cameras on the map for you. Travel safe and protect your pocket!</p>

					<div class="row margin-top60">

						<!-- INFO -->
						<div class="container">

							<div class="white-row">
								<div id="map"><!-- google map --></div>
								<script>
									function initMap() {
								        var map = new google.maps.Map(document.getElementById('map'), {
								          zoom: 11,
								          center: {lat: -34.92862119999999, lng: 138.5999594}
								        });

								        <?php

								        	date_default_timezone_set("Australia/Adelaide");

								          $conn = new mysqli('localhost', 'MichaelZhu', 'Zcc88929@', 'speed_camera_location_history');
								          if ($conn->connect_error)
								          {
								            die("Connection failed: " . $conn->connect_error);
								          }

								          $result = $conn->query("SELECT location FROM location_2016 ORDER BY id DESC LIMIT 1");

								          if ($result->num_rows > 0) 
								          {
								            while($row = $result->fetch_assoc())
								            {
								              echo $row["location"];
								            }
								          }
								          else 
								          {
								            echo "0 results";
								          }

								          $conn->close();

								        ?>

								        var geocoder = new google.maps.Geocoder();
								        
								        var i = 0;
								        var interval = setInterval(function () {

								            var counter = 0;
								            var done = geocodeAddress(geocoder, map, locations[i]);
								            while (done === 0)
								            {
								              counter++;
								              done = geocodeAddress(geocoder, map, locations[i]);

								              if(counter === 5)
								                alert(address + ' - cannot be added...');
								            }

								            i++;

								            if (i === locations.length) 
								              clearInterval(interval);

								        }, 500);
								      }
								</script>

								<div class="divider white half-margins"><!-- divider -->
									<i class="fa fa-star"></i>
								</div>

								<p class="nomargin-bottom">
									<span class="block"><strong><i class="fa fa-map-marker"></i> Address:</strong> Street Name, City Name, Country</span>
									<span class="block"><strong><i class="fa fa-phone"></i> Phone:</strong> 1800-555-1234</span>
									<span class="block"><strong><i class="fa fa-envelope"></i> Email:</strong> <a href="mailto:mail@yourdomain.com">mail@yourdomain.com</a></span>
								</p>

							</div>


						</div>
						<!-- /INFO -->

					</div>

				</div>
			</section>
			<!-- /CONTACT -->


			<!-- COUNTERS -->
			<section id="support" class="paddings styleBackground"><!-- add class: styleBackground -->
				<div class="container">
					<h1 class="margin-top30 margin-bottom80 text-center">Support us so we can serve you better!</h1>
					<div class="row text-center countTo">
						<div class="col-md-4">
							<strong data-to="9999">0</strong>
							<label class="padding20"><a class="btn btn-success btn-lg"><span class="glyphicon glyphicon-thumbs-up"></span> Helpful</a></label>
						</div>
						<div class="col-md-4">
							<strong data-to="1234">0</strong>
							<label class="padding20"><a class="btn btn-danger btn-lg"><span class="glyphicon glyphicon-heart"></span> Donate</a></label>
						</div>
						<div class="col-md-4">
							<strong data-to="4567">0</strong>
							<label class="padding20"><a class="btn btn-info btn-lg"><span class="glyphicon glyphicon-pencil"></span> Subscribe</a></label>
						</div>
					</div>
				</div>
			</section>
			<!-- /COUNTERS -->


			<!-- CONTACT -->
			<section id="contact" class="special-row padding100 margin-footer">
				<div class="container">

					<h1 class="text-center">Help us to <strong><em>improve</em></strong> or just say <strong><em>Hello!</em></strong></h1>
					<div class="divider"><!-- divider -->
						<i class="fa fa-star"></i>
					</div>

					<p class="lead">Please send us a note about how you'd like us to improve or just let us know we are helpful. We'll always be there for you!</p>

					<div class="row margin-top60">

						<!-- FORM -->
						<div class="container">

							<h2>Don't be shy! We never bite! <i class="fa fa-smile-o" aria-hidden="true"></i></h2>

							<form class="white-row" action="#" method="post">
								<div class="row">
									<div class="form-group">
										<div class="col-md-12">
											<label>Subject</label>
											<input type="text" value="" data-msg-required="Please enter the subject." maxlength="100" class="form-control" name="subject" id="subject">
										</div>
									</div>
								</div>
								<div class="row">
									<div class="form-group">
										<div class="col-md-12">
											<label>Message</label>
											<textarea maxlength="5000" data-msg-required="Please enter your message." rows="10" class="form-control" name="message" id="message"></textarea>
										</div>
									</div>
								</div>

								<br />

								<div class="row">
									<div class="col-md-12">
										<input type="submit" value="Send Message" class="btn btn-primary btn-lg" data-loading-text="Loading...">
									</div>
								</div>
							</form>
						
						</div>
						<!-- /FORM -->


						<!-- INFO -->
						<div class="col-md-4">

							<p class="nomargin-bottom">
								<span class="block"><strong><i class="fa fa-envelope"></i> Email:</strong> <a href="mailto:mail@yourdomain.com">mail@yourdomain.com</a></span>
							</p>

						</div>
						<!-- /INFO -->

					</div>

				</div>
			</section>
			<!-- /CONTACT -->

		</div>
		<!-- /WRAPPER -->



		<!-- FOOTER -->
		<footer>

			<!-- copyright , scrollTo Top -->
			<div class="footer-bar">
				<div class="container">
					<span class="copyright">Copyright &copy; I-Life Group. All Rights Reserved.</span>
					<a class="toTop" href="#topNav">BACK TO TOP <i class="fa fa-arrow-circle-up"></i></a>
				</div>
			</div>
			<!-- copyright , scrollTo Top -->


			<!-- footer content -->
			<div class="footer-content">
				<div class="container">

				</div>
			</div>
			<!-- footer content -->

		</footer>
		<!-- /FOOTER -->


		<!-- JAVASCRIPT FILES -->
		<script type="text/javascript" src="assets/plugins/jquery-2.1.4.min.js"></script>
		<script type="text/javascript" src="assets/plugins/jquery.easing.1.3.js"></script>
		<script type="text/javascript" src="assets/plugins/jquery.cookie.js"></script>
		<script type="text/javascript" src="assets/plugins/jquery.appear.js"></script>
		<script type="text/javascript" src="assets/plugins/jquery.isotope.js"></script>
		<script type="text/javascript" src="assets/plugins/masonry.js"></script>

		<script type="text/javascript" src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="assets/plugins/magnific-popup/jquery.magnific-popup.min.js"></script>
		<script type="text/javascript" src="assets/plugins/owl-carousel/owl.carousel.min.js"></script>
		<script type="text/javascript" src="assets/plugins/stellar/jquery.stellar.min.js"></script>
		<script type="text/javascript" src="assets/plugins/knob/js/jquery.knob.js"></script>
		<script type="text/javascript" src="assets/plugins/jquery.backstretch.min.js"></script>
		<script type="text/javascript" src="assets/plugins/superslides/dist/jquery.superslides.min.js"></script>
		<script type="text/javascript" src="assets/plugins/styleswitcher/styleswitcher.js"></script><!-- STYLESWITCHER - REMOVE ON PRODUCTION/DEVELOPMENT -->
		<script type="text/javascript" src="assets/plugins/mediaelement/build/mediaelement-and-player.min.js"></script>
		
		<!-- REVOLUTION SLIDER -->
		<script type="text/javascript" src="assets/plugins/revolution-slider/js/jquery.themepunch.tools.min.js"></script>
		<script type="text/javascript" src="assets/plugins/revolution-slider/js/jquery.themepunch.revolution.min.js"></script>
		<script type="text/javascript" src="assets/js/slider_revolution.js"></script>

		<script type="text/javascript" src="assets/js/scripts.js"></script>

		<!-- ONEPAGE NAVIGATION -->
		<script type="text/javascript" src="assets/plugins/jquery.nav.min.js"></script>
		<script type="text/javascript">
			jQuery('#topMain').onePageNav({
				currentClass: 'active',
				changeHash: false,
				scrollSpeed: 750,
				scrollThreshold: 0.5,
				filter: ':not(.external)',
				easing: 'easeInOutExpo'
			});
		</script>



		<!-- Google Analytics: Change UA-XXXXX-X to be your site's ID. Go to http://www.google.com/analytics/ for more information. -->
		<!--<script>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

			ga('create', 'UA-XXXXX-X', 'domainname.com');
			ga('send', 'pageview');
		</script>
		-->

	</body>
</html>