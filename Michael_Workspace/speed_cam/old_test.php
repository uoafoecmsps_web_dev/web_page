<!DOCTYPE html> 
<html>
  <head>
    <title>Fetch</title>
  </head> 
  <body>
    <?php

      date_default_timezone_set("Australia/Adelaide");

      $today = date('N', strtotime(date("l")));

      include_once './simplehtmldom_1_5/simple_html_dom.php';

      $html = file_get_html('https://www.police.sa.gov.au/your-safety/road-safety/traffic-camera-locations');
      $locations = $html->find('div[class=accordion]');
      $today_loc = $locations[$today-1]->next_sibling();
      
      // p is an array
      $loc = $today_loc->find('p');
      echo "var locations = [";
      foreach ($loc as $res) {
          echo '"' .  strip_tags($res) . '", '; 
      }
      echo "]";
      
    ?>
  </body>
</html>



<!-- REVOLUTION SLIDER -->
      <div class="fullscreenbanner-container">
        <div class="fullscreenbanner" >
          <ul>

            <!-- SLIDE  -->
            <li data-transition="fade" data-slotamount="7" data-masterspeed="1500" >
            <!-- MAIN IMAGE -->
            <img src="assets/images/demo/revolution_slider/furniturebg22.jpg"  alt="furniturebg22"  data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">
            <!-- LAYERS -->

            <!-- LAYER NR. 1 -->
            <div class="tp-caption large_bold_white skewfromrightshort customout"
              data-x="16"
              data-y="203" 
              data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
              data-speed="500"
              data-start="800"
              data-easing="Back.easeOut"
              data-endspeed="500"
              data-endeasing="Power4.easeIn"
              style="z-index: 2">Atropos Offer
            </div>

            <!-- LAYER NR. 2 -->
            <div class="tp-caption medium_light_white skewfromrightshort customout"
              data-x="19"
              data-y="282" 
              data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
              data-speed="300"
              data-start="900"
              data-easing="Back.easeOut"
              data-endspeed="500"
              data-endeasing="Power4.easeIn"
              style="z-index: 3">Deluxe
            </div>

            <!-- LAYER NR. 3 -->
            <div class="tp-caption large_bold_white skewfromleftshort customout"
              data-x="124"
              data-y="261" 
              data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
              data-speed="300"
              data-start="1000"
              data-easing="Back.easeOut"
              data-endspeed="500"
              data-endeasing="Power4.easeIn"
              style="z-index: 4">Bedroom
            </div>

            <!-- LAYER NR. 4 -->
            <div class="tp-caption small_light_white customin customout"
              data-x="23"
              data-y="345" 
              data-customin="x:0;y:100;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:1;scaleY:3;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:0% 0%;"
              data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
              data-speed="500"
              data-start="1100"
              data-easing="Power4.easeOut"
              data-endspeed="500"
              data-endeasing="Power4.easeIn"
              style="z-index: 5">Lorem ipsum dolor sit amet, consetetur sadipscing elitr,<br/> sed diam nonumy eirmod tempor invidunt ut labore et dolore magna <br/>aliquyam erat, sed diam voluptua. 
            </div>

            <!-- LAYER NR. 5 -->
            <div class="tp-caption roundedimage customin"
              data-x="right" data-hoffset="-50"
              data-y="center" data-voffset="0"
              data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
              data-speed="1000"
              data-start="1200"
              data-easing="Back.easeOut"
              data-endspeed="300"
              style="z-index: 6"><img src="assets/images/demo/revolution_slider/bed1.jpg" alt="">
            </div>

            <!-- LAYER NR. 6 -->
            <div class="tp-caption large_bg_black customin"
              data-x="right" data-hoffset="0"
              data-y="195" 
              data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
              data-speed="1000"
              data-start="1700"
              data-easing="Back.easeInOut"
              data-endspeed="300"
              style="z-index: 7">$ 568
            </div>

            <!-- LAYER NR. 7 -->
            <div class="tp-caption mediumwhitebg customin"
              data-x="right" data-hoffset="0"
              data-y="150" 
              data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
              data-speed="1000"
              data-start="1600"
              data-easing="Back.easeInOut"
              data-endspeed="300"
              style="z-index: 8">Matrimonial Bed
            </div>

            <!-- LAYER NR. 8 -->
            <div class="tp-caption customin"
              data-x="490"
              data-y="270" 
              data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
              data-speed="500"
              data-start="2000"
              data-easing="Power4.easeInOut"
              data-endspeed="300"
              data-linktoslide="next"
              style="z-index: 9"><img src="assets/images/demo/revolution_slider/rightarrow.png" alt="">
            </div>

            <!-- LAYER NR. 9 -->
            <div class="tp-caption customin"
              data-x="430"
              data-y="270" 
              data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
              data-speed="500"
              data-start="1900"
              data-easing="Power4.easeInOut"
              data-endspeed="300"
              data-linktoslide="prev"
              style="z-index: 10"><img src="assets/images/demo/revolution_slider/leftarrow.png" alt="">
            </div>
          </li>

            <!-- SLIDE  -->
            <li data-transition="zoomout" data-slotamount="7" data-masterspeed="1500" >
              <!-- MAIN IMAGE -->
              <img src="assets/images/demo/revolution_slider/restaurantbg.jpg"  alt="restaurantbg"  data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">
              <!-- LAYERS -->

              <!-- LAYER NR. 1 -->
              <div class="tp-caption large_bold_white skewfromrightshort customout"
                data-x="right" data-hoffset="-95"
                data-y="200" 
                data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                data-speed="500"
                data-start="1400"
                data-easing="Back.easeOut"
                data-endspeed="500"
                data-endeasing="Power4.easeIn"
                style="z-index: 2">Atropos Best
              </div>

              <!-- LAYER NR. 2 -->
              <div class="tp-caption large_bold_white skewfromleftshort customout"
                data-x="right" data-hoffset="-350"
                data-y="bottom" data-voffset="-370"
                data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                data-speed="300"
                data-start="1500"
                data-easing="Back.easeOut"
                data-endspeed="500"
                data-endeasing="Power4.easeIn"
                style="z-index: 3">Deal
              </div>

              <!-- LAYER NR. 3 -->
              <div class="tp-caption small_light_white customin customout"
                data-x="right" data-hoffset="-50"
                data-y="bottom" data-voffset="-263"
                data-customin="x:0;y:100;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:1;scaleY:3;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:0% 0%;"
                data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                data-speed="500"
                data-start="1600"
                data-easing="Power4.easeOut"
                data-endspeed="500"
                data-endeasing="Power4.easeIn"
                style="z-index: 4">Lorem ipsum dolor sit amet, consetetur sadipscing elitr,<br/> sed diam nonumy eirmod tempor invidunt ut labore et <br/> dolore magnaaliquyam erat, sed diam voluptua. 
              </div>

              <!-- LAYER NR. 4 -->
              <div class="tp-caption roundedimage customin customout"
                data-x="0"
                data-y="center" data-voffset="0"
                data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:5;scaleY:5;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                data-speed="1000"
                data-start="800"
                data-easing="Back.easeOut"
                data-endspeed="300"
                style="z-index: 5"><img src="assets/images/demo/revolution_slider/food1.jpg" alt="">
              </div>

              <!-- LAYER NR. 5 -->
              <div class="tp-caption large_bg_black customin"
                data-x="182"
                data-y="181" 
                data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                data-speed="1000"
                data-start="1200"
                data-easing="Back.easeInOut"
                data-endspeed="300"
                style="z-index: 6">$ 32
              </div>

              <!-- LAYER NR. 6 -->
              <div class="tp-caption largegreenbg customin"
                data-x="45"
                data-y="100" 
                data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                data-speed="1000"
                data-start="1100"
                data-easing="Back.easeInOut"
                data-endspeed="300"
                style="z-index: 7">Best Atropos Meat
              </div>

              <!-- LAYER NR. 7 -->
              <div class="tp-caption customin"
                data-x="785"
                data-y="455" 
                data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                data-speed="500"
                data-start="1800"
                data-easing="Power4.easeInOut"
                data-endspeed="300"
                data-linktoslide="next"
                style="z-index: 8"><img src="assets/images/demo/revolution_slider/rightarrow.png" alt="">
              </div>

              <!-- LAYER NR. 8 -->
              <div class="tp-caption customin"
                data-x="725"
                data-y="455" 
                data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                data-speed="500"
                data-start="1700"
                data-easing="Power4.easeInOut"
                data-endspeed="300"
                data-linktoslide="prev"
                style="z-index: 9"><img src="assets/images/demo/revolution_slider/leftarrow.png" alt="">
              </div>

              <!-- LAYER NR. 9 -->
              <div class="tp-caption medium_light_white customin"
                data-x="125"
                data-y="51" 
                data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                data-speed="1000"
                data-start="1000"
                data-easing="Back.easeInOut"
                data-endspeed="300"
                style="z-index: 10">Atropos Special Price
              </div>
            </li>

            <!-- SLIDE  -->
            <li data-transition="slideleft" data-slotamount="7" data-masterspeed="1500" >
              <!-- MAIN IMAGE -->
              <img src="assets/images/demo/revolution_slider/condoebg1.jpg"  alt="condoebg1"  data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">
              <!-- LAYERS -->

              <!-- LAYER NR. 1 -->
              <div class="tp-caption large_bold_white skewfromrightshort customout"
                data-x="247"
                data-y="128" 
                data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                data-speed="500"
                data-start="800"
                data-easing="Back.easeOut"
                data-endspeed="500"
                data-endeasing="Power4.easeIn"
                style="z-index: 2">New Affordable
              </div>

              <!-- LAYER NR. 2 -->
              <div class="tp-caption large_bold_white skewfromleftshort customout"
                data-x="right" data-hoffset="-249"
                data-y="131" 
                data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                data-speed="500"
                data-start="900"
                data-easing="Back.easeOut"
                data-endspeed="500"
                data-endeasing="Power4.easeIn"
                style="z-index: 3">Condos
              </div>

              <!-- LAYER NR. 3 -->
              <div class="tp-caption small_light_white customin customout"
                data-x="center" data-hoffset="0"
                data-y="center" data-voffset="-30"
                data-customin="x:0;y:100;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:1;scaleY:3;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:0% 0%;"
                data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                data-speed="500"
                data-start="1600"
                data-easing="Power4.easeOut"
                data-endspeed="500"
                data-endeasing="Power4.easeIn"
                style="z-index: 4">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod <br/>tempor invidunt ut labore et dolore magnaaliquyam erat, sed diam voluptua.
              </div>

              <!-- LAYER NR. 4 -->
              <div class="tp-caption roundedimage customin customout"
                data-x="100"
                data-y="bottom" data-voffset="0"
                data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:5;scaleY:5;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                data-speed="1000"
                data-start="1700"
                data-easing="Power4.easeInOut"
                data-endspeed="300"
                style="z-index: 5"><img src="assets/images/demo/revolution_slider/apartment3.jpg" alt="">
              </div>

              <!-- LAYER NR. 5 -->
              <div class="tp-caption customin"
                data-x="center" data-hoffset="30"
                data-y="50" 
                data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                data-speed="500"
                data-start="2300"
                data-easing="Power4.easeInOut"
                data-endspeed="300"
                data-linktoslide="next"
                style="z-index: 6"><img src="assets/images/demo/revolution_slider/rightarrow.png" alt="">
              </div>

              <!-- LAYER NR. 6 -->
              <div class="tp-caption customin"
                data-x="center" data-hoffset="-30"
                data-y="50" 
                data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                data-speed="500"
                data-start="2200"
                data-easing="Power4.easeInOut"
                data-endspeed="300"
                data-linktoslide="prev"
                style="z-index: 7"><img src="assets/images/demo/revolution_slider/leftarrow.png" alt="">
              </div>

              <!-- LAYER NR. 7 -->
              <div class="tp-caption medium_light_white customin customout"
                data-x="287"
                data-y="center" data-voffset="-119"
                data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                data-speed="1000"
                data-start="1100"
                data-easing="Back.easeInOut"
                data-endspeed="300"
                style="z-index: 8">We make Dreams come True
              </div>

              <!-- LAYER NR. 8 -->
              <div class="tp-caption roundedimage customin customout"
                data-x="center" data-hoffset="0"
                data-y="bottom" data-voffset="0"
                data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:5;scaleY:5;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                data-speed="1000"
                data-start="1800"
                data-easing="Power4.easeInOut"
                data-endspeed="300"
                style="z-index: 9"><img src="assets/images/demo/revolution_slider/apartment2.jpg" alt="">
              </div>

              <!-- LAYER NR. 9 -->
              <div class="tp-caption roundedimage customin customout"
                data-x="right" data-hoffset="-100"
                data-y="bottom" data-voffset="0"
                data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:5;scaleY:5;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                data-speed="1000"
                data-start="1900"
                data-easing="Power4.easeInOut"
                data-endspeed="300"
                style="z-index: 10"><img src="assets/images/demo/revolution_slider/apartment1.jpg" alt="">
              </div>

              <!-- LAYER NR. 10 -->
              <div class="tp-caption medium_bg_red customin customout"
                data-x="685"
                data-y="212" 
                data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                data-speed="1000"
                data-start="1200"
                data-easing="Back.easeInOut"
                data-endspeed="300"
                style="z-index: 11">Call: 0800 0987654332
              </div>
            </li>

            <!-- SLIDE  -->
            <li data-transition="flyin" data-slotamount="1" data-masterspeed="1500" >
              <!-- MAIN IMAGE -->
              <img src="assets/images/demo/revolution_slider/familybg.jpg"  alt="familybg"  data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">
              <!-- LAYERS -->

              <!-- LAYER NR. 1 -->
              <div class="tp-caption large_bold_white customin customout"
                data-x="center" data-hoffset="0"
                data-y="200" 
                data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                data-speed="1000"
                data-start="800"
                data-easing="Back.easeOut"
                data-endspeed="500"
                data-endeasing="Power4.easeIn"
                style="z-index: 2">Family Insurance
              </div>

              <!-- LAYER NR. 2 -->
              <div class="tp-caption largegreenbg customin customout"
                data-x="center" data-hoffset="0"
                data-y="center" data-voffset="-40"
                data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                data-speed="1000"
                data-start="900"
                data-easing="Back.easeOut"
                data-endspeed="500"
                data-endeasing="Power4.easeIn"
                style="z-index: 3">Atropos Best Service
              </div>

              <!-- LAYER NR. 3 -->
              <div class="tp-caption small_light_white customin customout"
                data-x="center" data-hoffset="20"
                data-y="bottom" data-voffset="-280"
                data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                data-speed="1000"
                data-start="1000"
                data-easing="Power4.easeInOut"
                data-endspeed="500"
                data-endeasing="Power4.easeInOut"
                style="z-index: 4">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod <br/>tempor invidunt ut labore et dolore magnaaliquyam erat, sed diam voluptua.
              </div>

              <!-- LAYER NR. 4 -->
              <div class="tp-caption customin"
                data-x="center" data-hoffset="30"
                data-y="50" 
                data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                data-speed="500"
                data-start="1800"
                data-easing="Power4.easeInOut"
                data-endspeed="300"
                data-linktoslide="next"
                style="z-index: 5"><img src="assets/images/demo/revolution_slider/rightarrow.png" alt="">
              </div>

              <!-- LAYER NR. 5 -->
              <div class="tp-caption customin"
                data-x="center" data-hoffset="-30"
                data-y="50" 
                data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                data-speed="500"
                data-start="1700"
                data-easing="Power4.easeInOut"
                data-endspeed="300"
                data-linktoslide="prev"
                style="z-index: 6"><img src="assets/images/demo/revolution_slider/leftarrow.png" alt="">
              </div>

            </li>
          
          </ul>
          <div class="tp-bannertimer"></div>  
        </div>
      </div>
      <!-- /REVOLUTION SLIDER -->