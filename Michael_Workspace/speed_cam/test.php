<!DOCTYPE html> 
<html>
  <head>
    <title>Testing</title>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <style>
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
      #gmap {
        height: 100%;
      }
      #floating-panel {
        position: absolute;
        top: 10px;
        left: 25%;
        z-index: 5;
        background-color: #fff;
        padding: 5px;
        border: 1px solid #999;
        text-align: center;
        font-family: 'Roboto','sans-serif';
        line-height: 30px;
        padding-left: 10px;
      }
    </style>
  </head> 
  <body>
    <div id="gmap"></div>
    <script>
      function initMap() {
        var map = new google.maps.Map(document.getElementById('gmap'), {
          zoom: 11,
          center: {lat: -34.92862119999999, lng: 138.5999594}
        });

        <?php

        	date_default_timezone_set("Australia/Adelaide");

          $conn = new mysqli('localhost', 'MichaelZhu', 'Zcc88929@', 'speed_camera_location_history');
          if ($conn->connect_error)
          {
            die("Connection failed: " . $conn->connect_error);
          }

          $result = $conn->query("SELECT location FROM location_2016 ORDER BY id DESC LIMIT 1");

          if ($result->num_rows > 0) 
          {
            while($row = $result->fetch_assoc())
            {
              echo $row["location"];
            }
          }
          else 
          {
            echo "0 results";
          }

          $conn->close();

        ?>

        var geocoder = new google.maps.Geocoder();
        
        var i = 0;
        var interval = setInterval(function () {

            var counter = 0;
            var done = geocodeAddress(geocoder, map, locations[i]);
            while (done === 0)
            {
              counter++;
              done = geocodeAddress(geocoder, map, locations[i]);

              if(counter === 5)
                alert(address + ' - cannot be added...');
            }

            i++;

            if (i === locations.length) 
              clearInterval(interval);

        }, 500);
      }

      function geocodeAddress(geocoder, resultsMap, address) {
        var done;
        geocoder.geocode({'address': address}, function(results, status) {
          if (status === google.maps.GeocoderStatus.OK) {
              //resultsMap.setCenter(results[0].geometry.location);
	            var marker = new google.maps.Marker({
	              	map: resultsMap,
	              	animation: google.maps.Animation.DROP,
	              	position: results[0].geometry.location
	            });

              marker.addListener('click', function() {
                resultsMap.setZoom(15);
                resultsMap.setCenter(marker.getPosition());
              });

              done = 1;
              //alert(address + ' is done...');
          } else {
            done = 0;
            //alert(address + ' - Geocode was not successful for the following reason: ' + status);
          }
        });
        return done;
      }
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=true&callback=initMap">
    </script>
  </body>
</html>